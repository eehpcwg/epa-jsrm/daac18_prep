Vendor support for EPA JSRM includes both software and hardware solutions.

Solutions for \textit{capping system-wide power consumption} focus on
ensuring that the total power consumption of an entire supercomputer
does not exceed a preconfigured limit. SchedMD's SLURM
scheduler~\cite{Slurm,Slurm_powerSaving_guide} provides mechanisms for
maintaining a power budget across a system by monitoring the power
draw of each system node and dynamically redistributing power as jobs
are launched and terminated. Tools from system integrators such as
Cray's CAPMC~\cite{martin2015cray} and System Management Workstation
(SMW)~\cite{cray_smw} enable system administrators to trigger various
actions such as automatically shutting down nodes upon detection of
system-wide power thresholds.

Some solutions provide \textit{idle node management} capabilities for
limiting the power consumed by idle resources. These capabilities
typically work by triggering the hardware to transition
into low power states (like C-states and S-states \cite{ACPIspec}).
SLURM, PBSPro, Moab, and CAPMC provide such
mechanisms.  One emerging issue related to this mechanism is the
sudden surge in power demand when a significant fraction of a large
machine is brought back into an active state. This power surge
leads to steep fluctuations in power demands which can be
undesirable for electricity service providers. Technologies
such as Cray's power staging tools are designed to reduce the
effects of high ramp rates in cases such as these.

\textit{Job-level management} solutions include monitoring and
managing the power and energy consumed by a given job.  The goal here
is to ensure efficient execution of applications.  To support this,
resource managers like SLURM, Moab, and Cray's ALPS provide mechanisms
for setting the CPU operating frequencies on all compute nodes
allocated to a given job.  The rationale is that running certain jobs
at lower frequencies may lead to significant power savings without
impacting performance.  However, this approach makes multiple
assumptions: (1) HPC applications have uniform load distribution
across all processes and threads, (2) there is zero manufacturing
variation among individual processing units, and (3) that computational
intensity remains constant throughout a job's execution.
In real world scenerios, these assumptions often do not hold.
Intel's Global Extensible Open Power Manager (GEOPM)
framework~\cite{eastep2017global} resolves these issues by enabling
automated and asynchronous load balancing and power steering among
multiple nodes servicing a given job.

At the hardware level, \textit{power monitoring and control} solutions
depend on features of the underlying system components.  The most
common examples of such features include Dynamic Voltage Frequency
Scaling (Intel, AMD, NVIDIA, IBM), Intel's RAPL (Running Average Power
Limiting)\cite{intel_dev_manual}, and out-of-band Baseboard Management Controller (BMC)
control mechanisms.
