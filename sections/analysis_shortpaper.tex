This section presents an in-depth analysis as well as insights about
the survey responses.  These contents extend the comprehensive
overview given in Table~1 of our previous work~\cite{maiterth2018}.
Our goal of this analysis is to identify commonalities among
approaches taken by the surveyed computing centers as well as to
identify particularly noteworthy approaches.

\begin{table*}[h]
  \begin{center}
    \caption{Total power and cooling budget with maximum, average and idle power draws (units are in MW)}
    \label{tab:pwr_consumption}
    \begin{tabular}{|c||c|c||c|c|c|}
      \hline
      \textbf{Center} & \textbf{Power Budget} & \textbf{Cooling Budget} & \textbf{Maximum draw} & \textbf{Average draw} & \textbf{Idle draw} \\
      \hline
      \hline
      \textbf{RIKEN} & 23 & 36 & 15 & 12 & 10 \\
      \textbf{TokyoTech} & 2 & 2 & 1.4 & 0.8 & 0.55 \\
      \textbf{CEA} & 10 & 7.5 & & 5 &  \\	
      \textbf{KAUST} & 3.6 & 2.9 & 3 & 2 & 0.55 \\
      \textbf{LRZ 1} & 10 & 10 & 2.9 & 2.2 & 0.7 \\
      \textbf{LRZ 2} & 10 & 10 & 1.5 & 1.2 & 0.4 \\
      \textbf{STFC} & 4.5 & 2 & 1 & & \\		
      \textbf{Trinity} & 19.2 &	27 & 8.4 & & 2.4 \\
      \textbf{Cineca} &	6 & & & & \\
      \textbf{JCAHPC} &	8 & 4.2 & 3.2 & 2.4 & \\	
      \hline
    \end{tabular}
  \end{center}
  \vspace{-1mm}
\end{table*}

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.85\linewidth]{images/pwr_consumption.pdf}
    \caption{Maximum, average and idle power consumption as percentage of total power budget}
    \label{fig:pwr_consumption}
  \end{center}
\end{figure}

Table~\ref{tab:pwr_consumption} in the current paper shows a
high-level summary of responses from the sites regarding power and
cooling capacity as well as maximum, average, and idle power draws
from each site.  Following from the data presented in the Table,
Figure~\ref{fig:pwr_consumption} presents a graph of the percentage of
total power capacity that is reached for each of maximum, average, and
idle power draw at each site.

In analyzing the responses to the survey, one way of dividing the responses
is into two groups: (1) techniques that primarily focus on managing power,
such as by limiting the impact of idle resources on a center's current power
draw, and (2) techniques that primarily focus on managing energy, such as
by evaluating the performance-per-Watt profile of each job and attempting to
determine a way of scheduling jobs to optimize this metric.  The following
subsections explore these delineations in greater detail.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Power-Oriented Approaches}
\label{sec:Analysis:Power}
The first broad category of \EPAJSRM techniques focuses on managing
power consumption.  In many ways, power-oriented techniques are more
straightforward because power consumption tends to be a more
short-term goal and, accordingly, the telemetry necessary to achieve
power-oriented goals can be taken from more ephemeral sources.  That said,
the power draw of a supercomputing center is in many cases the biggest
limiting factor to the maximimum size of computer that a center can
run, so managing power is a critical success factor for many centers.

In the simplest approach, the batch job scheduler combines information
about idle resources and upcoming jobs and uses the resource
management software to \textit{shut down the idle resources}.
This simple approach has two advantages.  First, powered off nodes
consume no (or essentially no) power.  Second, most job schedulers and
resource managers (e.g., Moab~\cite{Moab,moab_greenComputing} and
SLURM~\cite{Slurm, Slurm_powerSaving_guide}) support this
functionality.  However, despite the advantages related to simplicity
and effectiveness, shutting down idle resources is often not possible
for the simple fact that supercomputers typically have consistently
high utilization except during scheduled maintenance windows.  High
purchase costs and limited lifespans, typically around 60 months, mean
that supercomputing centers strive to keep all computing resources
highly utilized with few periods of idleness.

One survey site that uses the technique of shutting down idle
resources to significant effect is the Tokyo Institute of Technology.
The benefits of the approach can be observed in
Figure~\ref{fig:pwr_consumption} which shows the percentage of
maximum, average and, idle power draw.
%% , and in
%% Figure~\ref{fig:average_pwr} which shows the relationship of average
%% power draw to maximum power draw.
Tokyo Institute of Technology sees
high idle power draws, thus this technique outweighs the disadvantages
the approach can have and the center achieves the highest average
power draw compared to its maximum power draw.

%% \begin{figure}
%%   \begin{center}
%%     \includegraphics[width=0.85\linewidth]{images/average_pwr.pdf}
%%     \caption{Average power consumption as percentage of maximum power consumption}
%%     \label{fig:average_pwr}
%%   \end{center}
%% \end{figure}

Another power-oriented approach is \textit{dynamic job termination}.
Dynamic job termination is used to directly respond to power
constraint situations by terminating one or more running jobs in order
to keep the total system power draw under a defined critical value.
The power limit may be either a hard limit (e.g., an actual hardware
limits) or a soft limit (e.g., due to exceptional situation-based
power costs or due to a buffered hardware safeguard).  Dynamic job
termination approaches may vary in complexity from fully manual to
fully autonomous selection and cancellation of jobs.  Candidate jobs
for termination are usually selected based on each job's contribution
to overall power conumption.  Additional factors that may be considered
include the progress that each job has made toward its overall requested
wallclock time, priority or some time-critical designation of each job,
and whether each job is a piece of a longer-term multi-job workflow.
Generally, terminated jobs are re-queued for execution at a future
time when power demands are lower.

RIKEN is an example of a site that employs dynamic job termination.
When RIKEN's resource management system detects an excessive power
usage in relation to power available to the center, the resource
management system begins stopping jobs by canceling (not suspending)
the job using the largest number of nodes, repeating the process
until power consumption falls below the critical threshold.  Research
at the center is currently underway to develop a more sophisticated
policy based around some of the factors described in the previous
paragraph.

% NOTE from Greg on August 26, 2018:
% I am going to remove this single-sentence paragraph because the
% use of P-states, frequency limits, etc. is also described under
% energy-aware techniques below.
%
%% An alternative to outright terminating jobs in response
%% to a violation of a predefined critical system power
%% threshold, is to trigger the hardware to enter into a
%% low power states like P-states or strict frequency limits.


%Another approach is \textit{dynamic job termination}.  Dynamic job
%termination is a brute-force method of responding to power constraint
%situations by terminating one or more running jobs in order to keep
%the total system power draw under some predefined critical limit.
%After terminating jobs, the job scheduler may further reduce power
%consumption by shutting down now-idle resources as described above.
%
%The task of determining which jobs to terminate may be done manually
%by a human based on knowledge of the running workload or may be
%automated via software that interacts with the job scheduler.  Such
%software might select jobs to terminate by size (i.e., terminate the
%largest job until system power falls below the critical threshold), by
%running time (i.e., terminate the job that has been running for the
%least amount of time using the notion that this job has the least work
%that might be lost), or by active power telemetry that allows the job
%scheduler to estimate the power draw of each job and find a ``best
%fit'' of jobs to kill to reach the critical threshold.  In cases
%where a given job has the ability to checkpoint its work prior to
%being terminated, the job scheduler may take appropriate measures to
%properly signal the job accordingly prior to terminating the job,
%subject to the time requirements for writing a checkpoint file versus
%the time frame within which the system power draw must be brought under
%the critical threshold.
%
%Sites that employ dynamic job termination include RIKEN.  At RIKEN,
%when the resource management system detects an excessive power usage,
%it automatically stops jobs by canceling (not suspending) the job
%using the largest number of nodes, although research is currently
%being conducted into more sophisticated policies.
%
%%%%% This is not used anywhere in our survey [Matthias]
%An alternative to outright terminating jobs in response
%to a violation of a predefined critical system power
%threshold, is to trigger the hardware to enter into a
%low power states like P-states (P0--Pn) and C-states
%(C0--Cn). These
%states - as defined by the ACPI (Advanced Configuration and
%Power Interface) specification\cite{ACPIspec} - 
%map to different modes of operation of the
%components. 
%
%P-states correspond to discrete performance states of
%hardware that map to different voltage/frequency
%levels. The ability to control these states on-demand
%is called - Dynamic Voltage Frequency Scaling or DVFS.
%Using DVFS, voltage and frequency can be scaled down,
%resulting in a corresponding decrease in both power
%draw and the associated performance of the machine. For
%systems that expose these control knobs to the software
%stack, it may be possible to bring the overall power
%draw of the machine under a specified threshold,
%without resorting to outright termination of jobs. 
%
%Relative to P-states, C-states allow hardware
%components to enter into modes that are much
%lower-powered than P-states. Unlike a P-state where a
%hardware component is still operational, all C-states
%(except C0) correspond to hardware states where
%multiple subcomponents are completely turned off to
%achieve power-savings.  Therefore, triggering hardware
%components into low-powered C-states is a preferred
%approach for idle node power management. It must be
%noted, however, that switching between C-states comes
%at a cost: The transition latency between a low-powered
%non-operational C-state (C1--Cn) into a
%fully-operational state (C0) is significantly higher
%than that between two P-states. As a result, JSRM
%solutions that use C-states to limit idle-node power
%have to tolerate the penalty while switching the nodes
%to `low-powered idle' to `full-active' states during
%job allocations.
%
%No site reported using these hardware states with
%the explicit intention of tackling the violation of the
%system power threshold. That said, some sites
%like CEA, LRZ, and STFC reported using resource
%managers like Slurm and IBM Load Leveler that leverage
%these hardware states to limit the power
%consumption by idle and active nodes.
%Section~\ref{sec:Analysis:Active} below, describes the
%use of P-states (DVFS) as part of an overall strategy
%to reduce energy consumption of \textit{active} nodes
%over the longterm operation of a supercomputer.

%%%%% vvvv This is not used in any of our surveyed approaches! [Matthias]
%%%%%      This is even related work.
%The final approach discussed in this section is 
%\textit{static or dynamic resizing of job resources}.  In this
%approach, the total number of compute nodes allocated to a job is
%limited, either statically when the job is released from the batch
%queue and launched onto resources or dynamically while the job is
%running.  By reducing the number of resources allocated to a job, the
%resources immediately draw much less power than if they were actively
%engaged in executing a job.  Additionally, nodes that become
%unoccupied due to dynamically migrating work away could be candidates
%for shutdown as described previously.  This approach involves some
%knowledge of the workload to be effective.  For example, for the case
%of a traditional supercomputing workload consisting of tightly coupled
%scientific applications that are submitted into a batch system with a
%specific number of requested processing elements, it may not make
%sense to the problem's semantics for the batch system to allocate a
%different number of processing elements when the job is launched.
%These cases would be if the job requests and requires a specific
%number of processors, such as a power of two number of processors or a
%square or cube number of processors, in order to properly distribute
%the discrete pieces of the problem.  Advanced runtime systems such as
%Charm++ \cite{kale2012charm} that decouple the logical decomposition of a job from
%its physical resources could accommodate such static launch-time
%changes in the number of allocated resources as well as allowing
%dynamic run-time changes.  Furthermore, some centers that participated
%in the survey reported having some fraction of their total work-flow
%consumed by cloud-type jobs, and these are ideal candidates for
%dynamic resizing of job resources due to the elastic nature of these
%jobs.  For example, MapReduce jobs can be readily reduced in size by
%having the mapper reduce the number of candidate nodes onto which
%tasks are placed. That is to say, the inherent characteristics of
%the MapReduce programming model that make it fault tolerant can also
%be leveraged to allow it to accommodate the power-related mission of
%a supercomputing center.

% NOTE from Greg on August 26, 2018
% I removed the following small two-sentence paragraph because I do
% not think it adds anything we didn't say above.
%
%% While the strategies for energy and power aware job scheduling and
%% resource management that deal with characteristics of idle nodes are
%% typically straightforward to implement, their impact tends to be
%% limited to a supercomputing center's short-term objectives.
%% Approaches that have a more broad impact related to long-term energy
%% reduction are discussed in the next section.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Energy-Oriented Approaches}
\label{sec:Analysis:Energy}

The second broad category of \EPAJSRM techniques focuses on managing
energy.  Because supercomputing centers typically try to keep their
computational resources engaged at all times, energy-oriented
approaches are in some ways more aligned with overall center goals
because energy-oriented approaches tend to involve managing active
resources.  To this end, energy-oriented approaches generally involve
optimizing around some objective involving factors such as utilization,
performance, fairness, time to completion, and energy consumption.
Making these decisions often involves maintaining historic information
about job execution and combining this information with information
taken from the currently queued upcoming jobs as well as with information
about the center's longer term goals.

It is interesting to notice that optimizing toward energy use is often
the same as optimizing toward performance, and that these goals are in
many cases \emph{in opposition} to the goal of optimizing toward power
consumption.  Consider the well-understood case of optimizing a parallel
application's performance by load balancing processing elements across
the CPUs allocated to the job.  The typical outcome of load balancing is
that application performance is improved leading to less execution time
to complete the job.  This objective likely coincides with the objective
of improving the energy efficiency of the job due to the job's shorter
wallclock time to complete.  However, because load balancing typically
results in much higher CPU utilization on each CPU used in the job, the
peak power of the job may be much higher.

%
%The second broad category of energy and power aware job scheduling and
%resource management techniques focuses on managing active resources.
%Supercomputers tend to be quite expensive and, accordingly, sites that
%operate large supercomputers have an incentive to operate the machines
%in a state of high utilization where computational resources are
%nearly always active.  Typically, managing these active resources
%requires striking a balance among several constraints such as job
%performance, system utilization, and energy consumption.  Striking
%such a balance often requires taking a longer-term perspective in
%which techniques are applied over a substantial fraction of a center's
%workload and have an impact to a center's overall energy profile
%rather than to a point-in-time power consumption objective.

%% Matching power supply to power demand
%% GAK: Somebody put this in, but I don't know what it means.

As described above, \EPAJSRM techniques that are focused on energy
typically need to have some understanding of how a given job
configuration (executable, input data, number of nodes, node layout,
etc.) is expected to perform.  Accordingly, one approach for
energy-oriented job scheduling reported by several centers
is \textit{application tagging}.  With this approach, users provide
information to the job scheduler about characteristics of submitted
jobs by manually applying one or more ``tag values'' to each job.  The
job scheduler and resource management system use this information to
configure the allocated nodes accordingly.  Example tags can be
characteristics, such as CPU, memory, or I/O affinity of a job.
In other similar approaches, the tags that a user applies to a job
relate the job to historical records of past runs of the job.

%One approach to managing active resources is
%\textit{application tagging}.  With this approach, users inform the
%job scheduling and resource management system about characteristics
%of submitted jobs by manually applying one or more ``tag values'' to
%each job.  The job scheduling system can examine these tag values to
%adjust system configuration parameters accordingly.  For example, a
%job that is heavily I/O bound could be tagged as such by the user and
%the job scheduling system might launch the job onto nodes that have
%been adjusted to have a more energy efficient P-state using Dynamic
%Voltage and Frequency Scaling.  In cases of a I/O bound jobs, this
%P-state adjustment is unlikely to have a significant effect on job
%performance but could potentially significantly reduce the energy
%consumed by I/O bound jobs over a long period of time.  A challenge
%with this approach is that it depends on each user's ability to
%identify the important characteristics of each job to the job
%scheduler by applying appropriate tag values.

LRZ is perhaps the site that makes the most extensive use of
application tagging.  LRZ's approach was developed in collaboration
with IBM and leverages capabilities of the IBM LoadLeveler job
scheduler~\cite{loadLeveler} used on LRZ's SuperMUC system.  The
approach encourages users to tag their jobs at submit time with metadata
that allows the scheduler to choose the best DVFS settings for the
nodes allocated to a job.  The work involved development of models for
energy consumption for various applications running on SuperMUC under
various DVFS settings.  The concepts are explained in the work by
Auweter~et~al.~\cite{10.1007/978-3-319-07518-1_25} and involve an
initial run of each application with a low default frequency and
then setting the frequency for subsequent submission of the same
application at higher frequencies according to the model if the
energy/performance trade-off is favorable.  The effectiveness
of the technique is shown in Figure~\ref{fig:pwr_consumption}
where LRZ's maximum and average power draw, taken as a percentage of
total power capacity, are very similar.  The long-term effect of using
application tagging at LRZ is an overall reduction in energy
consumption.  Follow-on work at LRZ is planned to implement
similar functionality in the open source SLURM scheduler for
upcoming machines deployed at LRZ.

Approaches involving \textit{predictive models} extend application
tagging approaches by attempting to automatically tag jobs according
to specific characteristics of jobs that are typically stored in a
historical database of job runs.  These approaches are challenging
due to difficulties related to extrapolating historical experience
to new job configurations (e.g., different input datasets to the same
executable) and to unseen numbers or configurations of nodes.

% NOTE from Greg on August 26, 2018.
% I removed the following paragraph because I could not think of a way
% to substantiate it based on the survey responses.
%
%% Approaches in the active resource management currently evaluated contain
%% Energy and Power Aware Runtime systems, topology aware node-placement and
%% co-scheduling. These approaches are seen in research stages at the questioned 
%% centers and remain active research. Other ongoing evaluations are how 
%% energy and power aware job scheduling and resource management information can be
%% linked with IT infrastructure for improved efficiency.

%%%% [Matthias:] Also this is research and not observed in the wild.
%%%% Both paragraphs below could be included in the opportunities or related work?
%%%% Short version see abouve [Matthias/end]
%An extension of the application tagging approach is the use of
%\textit{predictive models} within the job scheduling and resource
%management system.  With this approach, the job scheduling system
%maintains a database of all job submissions for each user including
%characteristics such as job size (number of nodes or processing
%elements), total execution time, executable pathname, and
%input arguments.  In addition to this information, telemetry
%measurements such as node power measurements for the computing
%resources used by each job are recorded in the database.  Through
%regression analysis at the completion of each job, the job scheduling
%system can attempt to identify characteristics for each job, such as
%node DVFS settings, with which each job runs most energy efficiently.
%Upon subsequent submissions of a specific job, the optimal
%configuration can be applied automatically to the resources allocated
%to the job.  One challenge to this approach involves capturing and
%maintaining historical data for each batch job along with telemetry
%data for all computational resources.  A possibly bigger challenge,
%however, involves automatically recognizing the combination of
%executable, input arguments, and data files that constitutes a
%distinct instance of a job.  Consider cases where a user submits one
%job with a given executable and a second job with a different
%executable.  Assuming that these jobs would be completely different
%instances seems straightforward.  However, in cases where a user
%submits two jobs with the same executable but with different input
%datasets, the two jobs might perform similarly or might perform
%quite differently.  For example, the two jobs might use the same
%executable such as a molecular dynamics solver but the first input
%dataset might represent a very simple molecule to simulate while
%the second input dataset might represent a very complex model.
%At least one site, LRZ, reported that they were working toward
%objectives along this path.

%The final approach considered in this section is
%\textit{topology-aware job placement}.  This approach takes into
%consideration the communication patterns employed by a parallel application
%and attempts to place the processing elements of the job onto
%resources in such a way as to optimize communication.  In order for
%this placement to happen, the parallel runtime system layer must be
%instrumented to capture details of each message sent between each pair
%of processing elements in the job.  Then, in a subsequent off-line
%process after the job completes, this message trace is used to
%partition the communication graph either in terms of the number of
%messages passed between PEs or in terms of the total amount of message
%traffic passed between PEs.  The next time the same job type is
%submitted by the user (as above, determined either by requiring the
%user to identify the job using some kind of self-applied tagging
%mechanism or by the system attempting to automatically identify that a
%new job is likely a similar instance to a previously seen job) the job
%scheduler allocates resources and uses the resource manager to assign
%PEs to the resources in such a way as to optimize the communication
%graph.  For example, two PEs that communicate frequently would be
%placed onto nodes that are physically near each other
%on the network topology.  This approach
%works best for tightly-coupled parallel applications that are somewhat
%asymmetrical (e.g., unstructured mesh decomposition or
%particles-in-boxes decomposition) because this characteristic provides
%good places for partitioning the communication graph.  When
%successful, this approach can improve the energy consumption of a job
%directly and indirectly.  Energy consumption improves directly because
%less network hardware is energized in running the job.  Energy
%consumption improves indirectly due to improving performance of the
%job related to communication performance.  From the survey results,
%CEA reported that they use a form of topology-aware job
%placement as supported by SLURM.

%% Possibly talk about vendor support here.  The idea is that the
%% approaches described in this subsection are more sophisticated
%% and end up relying a lot more on vendor support to be successful.

%\input{sections/analysis/analysis_prelude}
%\input{sections/analysis/analysis_milos}

% FIXME: Decide what parts (if any) should go in?
%\input{sections/analysis/analysis_milos2}
